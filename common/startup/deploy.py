import datetime
import os
import re
import subprocess
import shutil
import tempfile
import time
try:
    from git import Repo
except ImportError:
    raise ImportError("Install gitpython for deploy")

try:
    BASH = "C:\\Users\\a.kot\\AppData\\Local\\Programs\\Git\\git-bash.exe"
except KeyError:
    raise SystemError("Install git (bash terminal) (ex. mingw for Windows) and set path to it to use git commands. "
                      "Example: %localappdata%\Programs\Git\git-bash.exe")

SSH_KEY = "%homepath%\.ssh\id_rsa"
RSPD_GIT = "git@gitlab.corp.geoscan.aero:cv/metashape/plugins.git"
PUBLIC_GIT = "git@github.com:geoscan/geoscan_plugins.git"

PRODUCTION_DIR = r"\\taiga\data\PS_scripts\ps_plugins\scripts"
BACKUP_DIR = r"\\taiga\data\PS_scripts\ps_plugins\backup_plugins"

MAKE_NSIS = r"C:\Program Files (x86)\NSIS\makensis.exe"


def deploy_new_version():
    time_start = time.time()
    print('Creating backup...')
    try:
        shutil.rmtree(BACKUP_DIR)
    except FileNotFoundError:
        pass

    shutil.copytree(PRODUCTION_DIR, BACKUP_DIR)

    print('Preparing production directory...')
    last_version = get_last_version(PRODUCTION_DIR)
    shutil.rmtree(PRODUCTION_DIR)

    print('Cloning git repository...')
    git_clone(git_repo=RSPD_GIT)

    print("Preparing for release...")
    remove_trash()
    create_version_file(last_version)

    # make_online_installer(last_version + 1)
    make_rsdp_plugins_installer(last_version + 1)
    # make_gnsspostprocessing_installer(last_version + 1)
    # make_roscartography_tools_installer(last_version + 1)
    # make_gazpromneft_installer(last_version + 1)
    # make_agp_installer(last_version + 1)
    # make_vsagp_installer(last_version + 1)

    print("Build {} released! Time: {} sec". format(last_version + 1, round(time.time() - time_start, 1)))


def deploy_new_public_version():
    print('Cloning data...')
    temp_dir = os.path.join(tempfile.gettempdir(),
                            'public_git_plugins_{}'.format(datetime.datetime.now().strftime("%Y_%m_%d_%H-%M-%S")),
                            'git_data')
    repo = Repo.clone_from(url=PUBLIC_GIT, to_path=temp_dir,
                           env={"GIT_SSH_COMMAND": 'ssh -i {}'.format(SSH_KEY)})
    repo.git.submodule('update', '--init', '--remote', '--recursive')

    print('Preparing data...')
    scripts_filtered = os.path.join(os.path.dirname(temp_dir), 'scripts')
    shutil.copytree(temp_dir, scripts_filtered, ignore=shutil.ignore_patterns('.git'))

    last_version = get_last_version(scripts_filtered)
    nsis_script = r"\\taiga\data\PS_scripts\ps_plugins\custom_builds_lists\public_plugins.nsi"
    installer_path = lambda build: \
        r"\\taiga\data\PS_scripts\installers\public_plugins\geoscan_plugins_public_build{}.exe".format(build)

    make_installer(
        nsis=MAKE_NSIS,
        installer_path=installer_path(last_version),
        nsis_script=nsis_script,
        source=os.path.dirname(temp_dir),
    )


def get_last_version(dir_):
    with open(os.path.join(dir_, 'version'), 'r') as file:
        version_str = file.read()

    try:
        version = re.search(r"\d+", version_str).group(0)
    except IndexError:
        raise IndexError("Couldn't parse version file")

    return int(version)


def git_clone(git_repo):
    temp_dir = os.path.join(tempfile.gettempdir(),
                            'git_plugins_{}'.format(datetime.datetime.now().strftime("%Y_%m_%d_%H-%M-%S")))
    repo = Repo.clone_from(url=git_repo, to_path=temp_dir,
                           env={"GIT_SSH_COMMAND": 'ssh -i {}'.format(SSH_KEY)})

    repo.git.submodule('update', '--init', '--remote', '--recursive')

    print("Copying cloned repository from TEMP...")
    shutil.copytree(temp_dir, PRODUCTION_DIR, ignore=shutil.ignore_patterns('.git'))


def remove_trash():
    files_to_remove = [
        '.gitignore',
        '.gitmodules',
        'proj_mini.install4j',
        'proj_offline.install4j',
        'test_all.py',
    ]
    for file in files_to_remove:
        path = os.path.join(PRODUCTION_DIR, file)
        if os.path.exists(path):
            os.remove(path)

    dirs_to_remove = []
    for dir in dirs_to_remove:
        path = os.path.join(PRODUCTION_DIR, dir)
        if os.path.exists(path):
            shutil.rmtree(path)


def create_version_file(last_version):
    with open(os.path.join(PRODUCTION_DIR, 'version'), 'w') as file:
        data = str("_{}\n".format(last_version + 1))
        file.write(data)


def create_branch_file():
    with open(os.path.join(PRODUCTION_DIR, 'branch.txt'), 'w') as file:
        file.write("master")


def make_installer(nsis, installer_path, nsis_script, source=None):
    if source is None:
        subprocess.run([nsis, '/XOutFile {}'.format(installer_path), nsis_script])
    else:
        subprocess.run([nsis,
                        '/XOutFile {}'.format(installer_path),
                        '/DSourceDir={}'.format(source),
                        nsis_script])

    installer_dir = os.path.dirname(installer_path)
    older_versions_path = os.path.join(installer_dir, "older_versions")
    if not os.path.exists(older_versions_path):
        os.makedirs(older_versions_path)

    for file in os.listdir(installer_dir):
        if os.path.isfile(os.path.join(installer_dir, file)) and file != os.path.basename(installer_path):
            os.replace(os.path.join(installer_dir, file), os.path.join(older_versions_path, file))


def make_rsdp_plugins_installer(build):
    nsis_script = r"\\taiga\data\PS_scripts\ps_plugins\custom_builds_lists\rsdp_plugins.nsi"
    installer_path = lambda build: \
        r"\\taiga\data\PS_scripts\installers\rsdp_plugins\geoscan_plugins_rsdp_build{}.exe".format(build)

    make_installer(
        nsis=MAKE_NSIS,
        installer_path=installer_path(build),
        nsis_script=nsis_script
    )


def make_gnsspostprocessing_installer(build):
    nsis_script = \
        r"\\taiga\data\PS_scripts\ps_plugins\custom_builds_lists\gnss_post_processing.nsi"
    installer_path = lambda build: \
        r"\\taiga\data\PS_scripts\installers\custom_builds\gnss_post_processing_only\gnss_post_processing_installer_build{}.exe".format(build)

    make_installer(
        nsis=MAKE_NSIS,
        installer_path=installer_path(build),
        nsis_script=nsis_script
    )


def make_roscartography_tools_installer(build):
    nsis_script = \
        r"\\taiga\data\PS_scripts\ps_plugins\custom_builds_lists\roscartography_tools.nsi"
    installer_path = lambda build: \
        r"\\taiga\data\PS_scripts\installers\custom_builds\roscartography_tools\roscartography_tools_build{}.exe".format(build)

    make_installer(
        nsis=MAKE_NSIS,
        installer_path=installer_path(build),
        nsis_script=nsis_script
    )


def make_gazpromneft_installer(build):
    nsis_script = \
        r"\\taiga\data\PS_scripts\ps_plugins\custom_builds_lists\gazpromneft.nsi"
    installer_path = lambda build: \
        r"\\taiga\data\PS_scripts\installers\custom_builds\gazpromneft\geoscan_plugins_gazpromneft_build{}.exe".format(build)

    make_installer(
        nsis=MAKE_NSIS,
        installer_path=installer_path(build),
        nsis_script=nsis_script
    )


def make_agp_installer(build):
    nsis_script = \
        r"\\taiga\data\PS_scripts\ps_plugins\custom_builds_lists\agp.nsi"
    installer_path = lambda build: \
        r"\\taiga\data\PS_scripts\installers\custom_builds\agp\geoscan_plugins_agp_build{}.exe".format(build)

    make_installer(
        nsis=MAKE_NSIS,
        installer_path=installer_path(build),
        nsis_script=nsis_script
    )


def make_vsagp_installer(build):
    nsis_script = \
        r"\\taiga\data\PS_scripts\ps_plugins\custom_builds_lists\vsagp.nsi"
    installer_path = lambda build: \
        r"\\taiga\data\PS_scripts\installers\custom_builds\vsagp\geoscan_plugins_vsagp_build{}.exe".format(build)

    make_installer(
        nsis=MAKE_NSIS,
        installer_path=installer_path(build),
        nsis_script=nsis_script
    )


if __name__ == '__main__':
    # deploy_new_version()
    deploy_new_public_version()
    # make_gazpromneft_installer(get_last_version())
    # make_offline_installer(get_last_version())
    # make_vsagp_installer(get_last_version())
    # make_rsdp_plugins_installer(550)
    pass

