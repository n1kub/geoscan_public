"""Common scripts, classes and functions

Copyright (C) 2021  Geoscan Ltd. https://www.geoscan.aero/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

import contextlib
import importlib
import logging
import json
import os
import shutil
import subprocess
import tempfile
import traceback
import zipfile
from pathlib import Path
from configparser import ConfigParser, NoOptionError
from distutils.dir_util import copy_tree
from distutils.version import LooseVersion

from PySide2.QtWidgets import *

import Metashape
from common.loggers.email_logger import send_geoscan_plugins_error_to_devs, LoggerValues
from common.utils.ui import ProgressBar, EndlessProgressBar
from common.startup.initialization import config, ps, update_sp_path
from common.startup.requirements_utils import PLUGINS_DIR, create_requirements, download_prebuilt_package, \
    download_file
from version import __version__

import locale
initial_locale = locale.getlocale()

try:
    import gitlab
except ModuleNotFoundError:
    default_sp_path = config.get('Paths', 'sp_path')
    python = config.get('Paths', 'python')
    subprocess.run([python, '-m', 'pip', 'install', '--upgrade', "--target={}".format(default_sp_path),
                    'python-gitlab==3.3.0'])
    import gitlab


def remove_existing_installation(name):
    """
    Disables and removes already installed plugin
    :param name: plugin name (without extension)
    """
    scripts = Path(config.get('Paths', 'local_app_data')) / 'scripts'

    try:
        shutil.rmtree(scripts / name)
    except FileNotFoundError:
        pass

    installed_plugin_path = scripts / 'installed_plugins' / f'{name}.py'
    installed_plugin_path.unlink()

    plugins_list_path = scripts / 'installed_plugins' / 'plugins.txt'
    with open(plugins_list_path, 'r+') as f:
        plugins = f.read().split('\n')
        plugins.remove(name)
        f.truncate(0)
        f.seek(0)
        for p in plugins:
            f.write(f'{p}\n')


def get_local_versions():
    """
    Collects versions of installed plugins
    :return: tuple of {name: version} pairs
    """
    local_versions = {}
    local_path = config.get('Paths', 'local_app_data')
    installed_plugins = (Path(local_path) / 'scripts' / 'installed_plugins' / 'plugins.txt').read_text().split('\n')

    for plugin in installed_plugins:
        try:
            plugin_path = Path(local_path) / 'scripts' / plugin / 'version.py'
            spec = importlib.util.spec_from_file_location("__version__", plugin_path)
            version_module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(version_module)
            local_versions[plugin] = version_module.__version__
        except FileNotFoundError:
            local_versions[plugin] = '0.0.0.1'

    return local_versions


def is_requirements():
    r = Path(config.get("Paths", "local_app_data")) / 'scripts' / 'requirements.txt'
    return r.exists()


def show_warning_yes_cancel(title, message):
    app = QApplication.instance()
    win = app.activeWindow()
    return QMessageBox.warning(win, title, message, QMessageBox.Ok | QMessageBox.Cancel)


def move_init_file(package_name):
    """
    Move init file from installation folder to installed_plugins and renames it
    :param package_name:
    """
    source_dir = config.get('Paths', 'scripts')
    installed_plugins_path = Path(source_dir) / 'installed_plugins'
    init_file_path = Path(source_dir) / package_name / f'init_{package_name}.py'
    init_file_path.replace(installed_plugins_path / f'{package_name}.py')


def specific_packages_exists():
    """
    Checks specific packages which cannot be downloaded from pip registry
    :return: True if packages exists, False if not
    """
    try:
        import gdal
        import numpy
        import shapely
        return True

    except ImportError:
        return False


def download_package(project, package, target_dir, format='tar.gz'):
    """
    Downloads a package from the GitLab project package registry using API
    :param project: GitLab Project object
    :param package: GitLab Package object which need to download
    :param target_dir: path to target directory
    :param format: extension of the file in package
    :return: path to downloaded file
    """
    if format == 'whl':
        file_name = f'{package.name}-{package.version}-cp38-cp38-win_amd64.{format}'
    else:
        file_name = f'{package.name}-{package.version}.{format}'

    print(f"[plugins] downloading {file_name}")

    dist = project.generic_packages.download(
        package_name=package.name,
        package_version=package.version,
        file_name=file_name
    )

    pkg_path = target_dir / file_name
    with open(pkg_path, 'wb') as f:
        f.write(dist)

    return pkg_path


def install_specific_package(project, package):
    """
    Downloads specific package and installs it into a site-packages directory
    :param project: GitLab Project object
    :param package: GitLab Package object which need to download
    """
    python = config.get('Paths', 'python')
    target_dir = Path(config.get('Paths', 'sp_path'))

    pkg_path = download_package(project, package, target_dir, format='whl')

    subprocess.run([python, '-m', 'pip', 'install', '--no-deps', '--target={}'
                   .format(target_dir), pkg_path])
    pkg_path.unlink()


def install_plugin(project, package):
    """
    Download or update plugin
    :param project: GitLab Project object
    :param package: GitLab Package object which contains plugin
    """
    try:
        remove_existing_installation(package.name)
    except (FileNotFoundError, ValueError):
        pass

    target_dir = Path(config.get('Paths', 'scripts'))
    python = config.get('Paths', 'python')
    
    pkg_path = download_package(project, package, target_dir)

    subprocess.run([python, '-m', 'pip', 'install', '--no-deps', '--target={}'
                   .format(target_dir), pkg_path])
    pkg_path.unlink()
    shutil.rmtree(target_dir / f'{package.name}-{package.version}.dist-info')

    move_init_file(package.name)

    installed_plugins = Path(target_dir) / 'installed_plugins' / 'plugins.txt'
    with open(installed_plugins, 'a+') as f:
        plugins = f.read().split('\n')
        if package.name not in plugins:
            f.write(f'\n{package.name}')

    if not config.has_option('Plugins', package.name):
        config.set('Plugins', package.name, 'True')

    print("[plugins] successfully downloaded")


def download_common(project):
    """
    Downloads and unpacks common files: startup, utils, etc.
    :param project: GitLab Project object
    """
    print('[plugins] updating common files')
    temp_zip = project.repository_archive(format='zip')  # загрузка архива в переменную
    source_code_zip = Path(config.get('Paths', 'resources')) / 'plugins_update.zip'
    with open(source_code_zip, "wb") as f:
        f.write(temp_zip)

    scripts_path = Path(config.get('Paths', 'scripts'))
    with zipfile.ZipFile(source_code_zip, 'r') as source:
        main_dir = source.infolist()[0].filename
        source.extractall(path=scripts_path)  # загрузка архива репозитория

    (scripts_path / main_dir / 'show_all.py').unlink()

    if (scripts_path / 'installed_plugins' / 'plugins.txt').exists():
        (scripts_path / main_dir / 'installed_plugins' / 'plugins.txt').unlink()

    if config.has_option('Storages', 'access_token'):
        new_config_path = scripts_path / main_dir / 'config.ini'
        new_config = ConfigParser()
        new_config.read(new_config_path)
        new_config.set('Storages', 'access_token', config.get('Storages', 'access_token'))
        with open(new_config_path, 'w') as f:
            new_config.write(f)

    copy_tree(scripts_path / main_dir, str(scripts_path))
    shutil.rmtree(scripts_path / main_dir)


def update_common(project):
    """
    Collects local and remote versions and updates common files if it needed
    :param project: GitLab Project object
    """
    try:
        version_file = project.files.get(file_path='version.py', ref='master')
        file_path = Path(config.get('Paths', 'scripts')) / 'remote_version.py'
        with open(file_path, 'wb') as f:
            f.write(version_file.decode())

        spec = importlib.util.spec_from_file_location("__version__", file_path)
        version_module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(version_module)
        remote_version = version_module.__version__
        file_path.unlink()

    except gitlab.exceptions.GitlabGetError:
        remote_version = '0.0.0.1'

    except Exception:
        remote_version = '0.0.0.1'
        sec, opt = 'Options', 'report_about_errors'
        if config.has_option(sec, opt) and config.get(sec, opt) == 'True':
            values = LoggerValues(input={}, plugin_name='main')
            send_geoscan_plugins_error_to_devs(error=traceback.format_exc(), values=values)

    print(f'[plugins] remote version: {remote_version}')
    local_version = __version__         # from local scripts/version.py file
    if LooseVersion(remote_version) > LooseVersion(local_version):
        download_common(project)


def update_plugins():
    """
    Checks both repositories (if access was granted) and downloads newer packages
    """
    print(f'[plugins] local version: {__version__}')
    update_sp_path()
    if not Path(config.get('Paths', 'resources')).exists():
        update_resources()
    specific_packages = ['numpy+mkl', 'GDAL', 'Shapely']

    if config.get('Options', 'auto_update') == 'True':
        local_versions = get_local_versions()
        for storage_type in ['public', 'private']:
            update_specific_packages = not specific_packages_exists()
            user_answer = None
            try:
                project_id = config.get('Storages', f'{storage_type}_storage_id')

                if storage_type == 'private':
                    gl = gitlab.Gitlab(url='https://gitlab.corp.geoscan.aero',
                                       private_token=config.get('Storages', 'access_token'))
                    gl.auth()
                else:
                    gl = gitlab.Gitlab()

                project = gl.projects.get(project_id)
                update_common(project)
                packages = project.packages.list(all=True)

                total = len([pkg for pkg in packages if pkg.name not in local_versions or \
                             LooseVersion(pkg.version) > LooseVersion(local_versions[pkg.name])])  # count of updates

                if not update_specific_packages:       # because specific packages always not in local_versions
                    total -= len(specific_packages)
                else:
                    progress = ProgressBar(_("Updating specific packages"))
                    cnt = 0
                    for pkg in packages:
                        if pkg.name in specific_packages:
                            install_specific_package(project, pkg)
                            cnt += 1
                            progress.update(cnt / len(specific_packages) * 100)
                    total -= len(specific_packages)

                for pkg in packages:
                    if pkg.name not in specific_packages and (pkg.name not in local_versions or
                                                              LooseVersion(pkg.version) > LooseVersion(local_versions[pkg.name])):

                        if user_answer is None:
                            print(f'[plugins] {total} updates was found')
                            title = _('Agisoft Metashape plugins')
                            message = _(f'Do you want to update {storage_type} plugins?')
                            app = QApplication.instance()
                            win = app.activeWindow()
                            ret = QMessageBox.warning(win, title, message, QMessageBox.Yes | QMessageBox.No)
                            if ret == QMessageBox.No or ret == QMessageBox.StandardButton.NoButton:
                                user_answer = False
                                continue
                            update_resources()
                            progress = ProgressBar(_(f"Updating {storage_type} plugins"))
                            cnt = 0
                            user_answer = True

                        elif user_answer == False:
                            continue

                        install_plugin(project, pkg)
                        cnt += 1
                        progress.update(cnt / total * 100)
                        local_versions[pkg.name] = pkg.version

            except NoOptionError:
                print(f'[plugins] {storage_type} storage for update not found')

            except gitlab.exceptions.GitlabAuthenticationError:
                print('[plugins] storage access token is invalid')

            except Exception:
                print(traceback.format_exc())
                sec, opt = 'Options', 'report_about_errors'
                if config.has_option(sec, opt) and config.get(sec, opt) == 'True':
                    values = LoggerValues(input={}, plugin_name='main')
                    send_geoscan_plugins_error_to_devs(error=traceback.format_exc(), values=values)
            
    check_and_install_sitepackages(online=True)
    check_and_install_sitepackages(online=False)


def check_and_install_sitepackages(online: bool):
    create_requirements()

    default_sp_path = config.get('Paths', 'sp_path')
    pip_repo_offline = Path(config.get('Paths', 'scripts')) / 'local_repo'
    update_site_packages_window = True
    for pip_repo, status in [(PLUGINS_DIR, 'online'), (pip_repo_offline, 'offline')]:
        if (online and status == 'offline') or (not online and status == 'online'):
            continue

        try:
            installed, needed_packages, outdated, requirements = check_pip_repo(pip_repo)
        except FileNotFoundError:
            continue

        if not needed_packages.issubset(installed) or outdated:
            if update_site_packages_window:
                update_site_packages_window = False
                ret = show_warning_yes_cancel(_("Python packages update is required (Internet connection is required)."),
                                              _("Please, close all other Agisoft Metashape applications or click Cancel."))
                if ret == QMessageBox.Cancel:
                    return

            clean_old_packages(outdated)
            needed_packages.difference_update(installed)
            needed_packages = needed_packages.union(outdated)

            if status == 'online':
                with open(Path(PLUGINS_DIR) / 'prebuilt_requirements.json', 'r') as file:
                    prebuilt_packages = json.load(file)
            else:
                prebuilt_packages = dict()

            print("Installing {}".format(" ".join(needed_packages)))
            ret = True
            ps.app.update()
            progress = ProgressBar(_("Installing site-packages"))
            try:
                python = config.get('Paths', 'python')
                for idx, package in enumerate(sorted(list(needed_packages))):
                    progress.update(idx / len(needed_packages) * 100, text=_("Installing site-packages") + ': ' + package)
                    if status == 'online' and package not in prebuilt_packages:
                        p = package if not requirements[package] else "{}=={}".format(package, requirements[package])
                        subprocess.run([python, '-m', 'pip', 'install',
                                        '--upgrade', "--target={}".format(default_sp_path), p])
                    elif status == 'online' and package in prebuilt_packages:
                        whl_file = download_prebuilt_package(package=package, prebuilt_packages=prebuilt_packages)
                        subprocess.run([python, '-m', 'pip', 'install',
                                        '--upgrade', "--target={}".format(default_sp_path), whl_file])
                        try:
                            Path(whl_file).unlink()
                        except Exception:
                            pass
                    elif status == 'offline':
                        subprocess.run([python, '-m', 'pip', 'install', '--upgrade',
                                        "--target={}".format(default_sp_path), "--no-index",
                                        "--find-links={}".format(pip_repo), package])
                    else:
                        raise SystemError
                # pip resets locale, we set it back
            except:
                logging.critical("Error installing with pip")
                traceback.print_exc()
                ret = False
            finally:
                locale.setlocale(locale.LC_ALL, initial_locale)
                progress.close()

    return False


def clean_old_packages(outdated):
    default_sp_path = config.get('Paths', 'sp_path')
    for f in os.listdir(default_sp_path):
        for o in outdated:
            if o in f.lower():
                # it stores every version of every package installed so we need to delete them manually
                path = os.path.join(default_sp_path, f)
                if os.path.isdir(path):
                    shutil.rmtree(path)
                else:
                    os.remove(path)
            # sometimes we have naming errors, every package should be checked
            if o == 'tensorflow-gpu':
                if 'tensorflow_gpu' in f:
                    shutil.rmtree(os.path.join(default_sp_path, f))         #


def check_pip_repo(pip_repo):
    from importlib import metadata as importlib_metadata
    import warnings
    warnings.filterwarnings("ignore")

    if not os.path.exists(os.path.join(pip_repo, "requirements.txt")):
        raise FileNotFoundError
    # list outdated packages
    python = config.get('Paths', 'python')
    with tempfile.TemporaryFile('w+') as temp:
        with contextlib.redirect_stdout(temp):
            subprocess.run([python, '-m', 'pip', 'list', '-o', "--no-index", "--find-links={}".format(pip_repo)])
        temp.seek(0)
        outdated = set(l.lower().split()[0] for l in temp.readlines())
        locale.setlocale(locale.LC_ALL, initial_locale)

    installed = importlib_metadata.distributions()
    installed = [p.metadata["Name"].lower() for p in installed if p.metadata["Name"]]

    requirements = dict()
    with open(Path(pip_repo) / 'requirements.txt') as f:
        needed_packages = set()
        for line in f.readlines():
            data = [x.strip().lower() for x in line.split('==')]
            package = data[0] if data[0] not in ['', '\n'] else None
            version = data[1] if len(data) == 2 else None
            if package:
                requirements[package] = version
                needed_packages.add(package)

    outdated = set(o for o in outdated if o in needed_packages)

    return installed, needed_packages, outdated, requirements


def update_resources():
    resources_url = config.get('Paths', 'binaries')
    resources_zip = download_file(url=resources_url, name='resources.zip',
                                  progress=True, progress_label=_("Update resources"))

    with zipfile.ZipFile(resources_zip, 'r') as zip_ref:
        zip_ref.extractall(config.get('Paths', 'local_app_data'))

    try:
        Path(resources_zip).unlink()
    except FileNotFoundError:
        pass
