"""Common scripts, classes and functions

Copyright (C) 2021  Geoscan Ltd. https://www.geoscan.aero/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

# not used in repo

import json
import os
from common.qt_wrapper.qt_wrapper import Window, TextEdit, Button
# from common.startup.initialization import config


class ChangeLogInformer(Window):
    """Class to initiliaze, save, load and visualize change log. Source - json file."""

    # path = os.path.join(config.get('Paths', 'local_app_data'), 'scripts', 'change_log.json')
    path = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'change_log.json')

    def __init__(self, text: str, build: int):
        super().__init__(title='Change log', size=(300, 400))
        self.insert_text_label('New in build {}:'.format(build))
        self.info = self.add_unit(TextEdit)
        self.info.set_value(text)
        self.info._text_field.setReadOnly(True)
        self.add_unit(Button, _("Ok"), self.close)

    @classmethod
    def get_info(cls, build):
        if not os.path.exists(cls.path):
            return None

        with open(cls.path, 'r') as file:
            data = json.load(file)

        return data[build]

    @classmethod
    def add_build_info(cls, text: str, build: int):
        with open(cls.path, 'r') as file:
            data = json.load(file)

        if build in data:
            raise ValueError
        data[build] = text

        with open(cls.path, 'w') as file:
            json.dump(data, file, indent=4, sort_keys=True)

    @classmethod
    def create_empty_change_log(cls):
        with open(cls.path, 'w') as file:
            json.dump({}, file, indent=4, sort_keys=True)


if __name__ == '__main__':
    ChangeLogInformer.create_empty_change_log()
    ChangeLogInformer.add_build_info(text='new!', build=552)
